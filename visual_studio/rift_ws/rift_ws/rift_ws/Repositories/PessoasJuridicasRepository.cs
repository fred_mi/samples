﻿using Dapper;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using rift_ws.Models;
using System;
using System.Data;

namespace rift_ws.Repositories
{
    public class PessoasJuridicasRepository 
    {
        IConfiguration configuration;
        public PessoasJuridicasRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public IConfiguration GetConfiguration()
        {
            return configuration;
        }

        public object Consultar_pessoa_juridica(int id = 0, string razao_social = "", string cnpj = "", string fantasia = "")
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {
                    cnpj = Utils.SomenteNumeros(cnpj);

                    if (id == 0 && string.IsNullOrWhiteSpace(cnpj) && string.IsNullOrWhiteSpace(fantasia)
                        && string.IsNullOrWhiteSpace(razao_social))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar ao menos um parâmetro: ID, RAZAO_SOCIAL, CNPJ ou FANTASIA." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select  pj.id, pj.cnpj, pj.razao_social, pj.fantasia, pj.endereco, " +
                                    "(select group_concat(fone separator ', ') from rift.fones_pessoa_juridica f where f.id_pessoa_juridica = pj.id) fones, " +
                                    "(select group_concat(email separator ', ') from rift.emails_pessoa_juridica f where f.id_pessoa_juridica = pj.id) emails " +
                                    "from rift.pessoa_juridica pj " +
                                    "where ";
                        var tem_param = false;

                        if (id != 0)
                        {
                            query = query + " pj.id = @id";
                            dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (!string.IsNullOrWhiteSpace(razao_social))
                        {
                            query = query + (tem_param ? " and " : "") + " upper(pj.razao_social) like upper(@razao_social)";
                            dyParam.Add("@razao_social", "%" + razao_social + "%", DbType.AnsiString, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (!string.IsNullOrWhiteSpace(cnpj))
                        {
                            query = query + (tem_param ? " and " : "") + " pj.cnpj = @cnpj";
                            dyParam.Add("@cnpj", cnpj, DbType.AnsiString, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (!string.IsNullOrWhiteSpace(fantasia))
                        {
                            query = query + (tem_param ? " and " : "") + " upper(pj.fantasia) like upper(@fantasia)";
                            dyParam.Add("@fantasia", "%" + fantasia + "%", DbType.AnsiString, ParameterDirection.Input);
                            tem_param = true;
                        }


                        query = query + " order by pj.razao_social";

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Incluir_pessoa_juridica(string cnpj = "", string razao_social = "", string fantasia = "", string endereco = "")
        {
            object result = null;

            cnpj = Utils.SomenteNumeros(cnpj);

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(cnpj) || string.IsNullOrWhiteSpace(razao_social))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: CNPJ e RAZAO_SOCIAL." });
                    }

                    if (!Utils.IsCnpj(cnpj))
                    {
                        return ("", new { Erro = true, Mensagem = "CNPJ inválido." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        // Novo grupo sempre inicia como Inativo
                        var query = "insert into rift.pessoa_juridica (cnpj, razao_social, fantasia, endereco) " +
                                    "values (@cnpj, @razao_social, @fantasia, @endereco)";

                        dyParam.Add("@cnpj", cnpj, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@razao_social", razao_social, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@fantasia", fantasia, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@endereco", endereco, DbType.AnsiString, ParameterDirection.Input);

                        SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        query = "select last_insert_id() id";
                        var v_id = SqlMapper.QuerySingle(conn, query, null, commandType: CommandType.Text);

                        result = new { ID = v_id.id };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Excluir_pessoa_juridica(int id = 0)
        {
            object result = null;
            IDbTransaction transaction = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        transaction = conn.BeginTransaction();

                        var query = "delete from rift.emails_pessoa_juridica where id_pessoa_juridica = @id ";
                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        query = "delete from rift.fones_pessoa_juridica where id_pessoa_juridica = @id ";
                        linhasafetadas = linhasafetadas + SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        query = "delete from rift.pessoa_juridica where id = @id ";
                        linhasafetadas = linhasafetadas + SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        transaction.Commit();

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Atualizar_pessoa_juridica(int id = 0, string cnpj = "", string razao_social = "", string fantasia = "", string endereco = "")
        {
            object result = null;

            cnpj = Utils.SomenteNumeros(cnpj);

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {
                    if (id == 0 || string.IsNullOrWhiteSpace(cnpj) || string.IsNullOrWhiteSpace(razao_social) 
                        || string.IsNullOrWhiteSpace(fantasia) || string.IsNullOrWhiteSpace(endereco))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID, RAZAO_SOCIAL, FANTASIA, ENDERECO" });
                    }

                    if (!Utils.IsCnpj(cnpj))
                    {
                        return ("", new { Erro = true, Mensagem = "CNPJ inválido." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "update rift.pessoa_juridica set cnpj = @cnpj, razao_social = @razao_social, fantasia = @fantasia, endereco = @endereco  where id  = @id ";

                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        dyParam.Add("@cnpj", cnpj, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@razao_social", razao_social, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@fantasia", fantasia, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@endereco", endereco, DbType.AnsiString, ParameterDirection.Input);

                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Incluir_email_pj(int id_pessoa_juridica = 0, string email = "")
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {
                try
                {
                   
                    if (id_pessoa_juridica == 0|| string.IsNullOrWhiteSpace(email))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID_PESSOA_JURIDICA e EMAIL." });
                    }

                    if (!Utils.ValidarEmail(email))
                    {
                        return ("", new { Erro = true, Mensagem = "E-mail inválido." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        // Novo grupo sempre inicia como Inativo
                        var query = "insert into rift.emails_pessoa_juridica (id_pessoa_juridica, email) " +
                                    "values (@id_pessoa_juridica, @email)";

                        dyParam.Add("@id_pessoa_juridica", id_pessoa_juridica, DbType.Int32, ParameterDirection.Input);
                        dyParam.Add("@email", email, DbType.AnsiString, ParameterDirection.Input);

                        SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        query = "select last_insert_id() id";
                        var v_id = SqlMapper.QuerySingle(conn, query, null, commandType: CommandType.Text);

                        result = new { ID = v_id.id };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Incluir_fone_pj(int id_pessoa_juridica = 0, string fone = "")
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {
                try
                {

                    if (id_pessoa_juridica == 0 || string.IsNullOrWhiteSpace(fone))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID_PESSOA_JURIDICA e FONE." });
                    }


                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        // Novo grupo sempre inicia como Inativo
                        var query = "insert into rift.fones_pessoa_juridica (id_pessoa_juridica, fone) " +
                                    "values (@id_pessoa_juridica, @fone)";

                        dyParam.Add("@id_pessoa_juridica", id_pessoa_juridica, DbType.Int32, ParameterDirection.Input);
                        dyParam.Add("@fone", fone, DbType.AnsiString, ParameterDirection.Input);

                        SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        query = "select last_insert_id() id";
                        var v_id = SqlMapper.QuerySingle(conn, query, null, commandType: CommandType.Text);

                        result = new { ID = v_id.id };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Excluir_email_pj(int id = 0)
        {
            object result = null;
            IDbTransaction transaction = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        transaction = conn.BeginTransaction();

                        var query = "delete from rift.emails_pessoa_juridica where id = @id ";
                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        transaction.Commit();

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Excluir_fone_pj(int id = 0)
        {
            object result = null;
            IDbTransaction transaction = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        transaction = conn.BeginTransaction();

                        var query = "delete from rift.fones_pessoa_juridica where id = @id ";
                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        transaction.Commit();

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Consultar_fones_pessoa_juridica(int id_pessoa_juridica = 0)
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id_pessoa_juridica == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar ao menos um parâmetro: ID_PESSOA_JURIDICA" });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select f.id, f.id_pessoa_juridica, f.fone " +
                                     "from rift.fones_pessoa_juridica f " +
                                     "where f.id_pessoa_juridica = @id_pessoa_juridica";

                        dyParam.Add("@id_pessoa_juridica", id_pessoa_juridica, DbType.Int32, ParameterDirection.Input);

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message }); 
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Consultar_emails_pessoa_juridica(int id_pessoa_juridica = 0)
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id_pessoa_juridica == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar ao menos um parâmetro: ID_PESSOA_JURIDICA" });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select e.id, e.id_pessoa_juridica, e.email " +
                                    "from rift.emails_pessoa_juridica e " +
                                    "where e.id_pessoa_juridica = @id_pessoa_juridica";

                        dyParam.Add("@id_pessoa_juridica", id_pessoa_juridica, DbType.Int32, ParameterDirection.Input);

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }


    }


}
﻿using Dapper;
using rift_ws.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace rift_ws.Repositories
{
    public class UsuariosRepository
    {
        IConfiguration configuration;
        public UsuariosRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public IConfiguration GetConfiguration()
        {
            return configuration;
        }

        public object Login_usuario(string login = "", string senha = "")
        {
            object result = null;
            string senha_valida = "";
            long id = 0;

            IDbTransaction transaction = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {
                    if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(senha))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: LOGIN e SENHA." });
                    }

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select u.id, u.senha from rift.usuario u " +
                                    "where u.login = lower(@login)";

                        var dyParam = new DynamicParameters();
                        dyParam.Add("@login", login, DbType.AnsiString, ParameterDirection.Input);


                        try
                        {
                            var usuario = SqlMapper.QuerySingle(conn, query, param: dyParam, commandType: CommandType.Text);
                            senha_valida = usuario.senha;
                            id = usuario.id;
                        }
                        catch (Exception e)
                        {
                            return ("", new { Erro = true, Mensagem = "Usuário não localizado ou senha inválida." + e.Message});
                        }

                    }

                    try
                    {
                        transaction = conn.BeginTransaction();

                        if (senha_valida == Utils.getHash(senha))
                        {
                            string token = Utils.getHash(senha_valida + DateTime.Now.ToString() + DateTime.Now.ToString());

                            var query = "update rift.usuario set token = @token, val_token = date_add(now(), interval 30 minute) " +
                                        "where id = @id  ";

                            var dyParam = new DynamicParameters();
                            dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                            dyParam.Add("@token", token, DbType.AnsiString, ParameterDirection.Input);

                            var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                            transaction.Commit();

                            result = new { id, token };
                        }
                        else
                        {

                            return ("", new { Erro = true, Mensagem = "Usuário não localizado ou senha inválida." });
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();

                        return ("", new { Erro = true, Mensagem = ex.Message });
                    }

                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

    }

}
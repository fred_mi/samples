﻿using Dapper;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using rift_ws.Models;
using System;
using System.Data;

namespace rift_ws.Repositories
{
    public class PessoasFisicasRepository 
    {
        IConfiguration configuration;
        public PessoasFisicasRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public IConfiguration GetConfiguration()
        {
            return configuration;
        }

        public object Consultar_pessoa_fisica(int id = 0, string nome = "", string cpf = "", string rg = "")
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {
                    cpf = Utils.SomenteNumeros(cpf);
                    rg = Utils.SomenteNumeros(rg);

                    if (id == 0 && string.IsNullOrWhiteSpace(cpf) && string.IsNullOrWhiteSpace(rg)
                        && string.IsNullOrWhiteSpace(nome))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar ao menos um parâmetro: ID, NOME, CPF ou RG." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select  pf.id, pf.cpf, pf.nome, pf.sexo, pf.data_nasc, pf.rg, pf.endereco, " +
                                    "(select group_concat(fone separator ', ') from rift.fones_pessoa_fisica f where f.id_pessoa_fisica = pf.id) fones, " +
                                    "(select group_concat(email separator ', ') from rift.emails_pessoa_fisica f where f.id_pessoa_fisica = pf.id) emails " +
                                    "from rift.pessoa_fisica pf " +
                                    "where ";
                        var tem_param = false;

                        if (id != 0)
                        {
                            query = query + " pf.id = @id";
                            dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (!string.IsNullOrWhiteSpace(nome))
                        {
                            query = query + (tem_param ? " and " : "") + " upper(pf.nome) like upper(@nome)";
                            dyParam.Add("@nome", "%" + nome + "%", DbType.AnsiString, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (!string.IsNullOrWhiteSpace(cpf))
                        {
                            query = query + (tem_param ? " and " : "") + " pf.cpf = @cpf";
                            dyParam.Add("@cpf", cpf, DbType.AnsiString, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (!string.IsNullOrWhiteSpace(rg))
                        {
                            query = query + (tem_param ? " and " : "") + " pf.rg = @rg";
                            dyParam.Add("@rg", rg, DbType.AnsiString, ParameterDirection.Input);
                            tem_param = true;
                        }

                        query = query + " order by pf.nome";

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Incluir_pessoa_fisica(string cpf = "", string nome = "", string sexo = "", string data_nasc = "", string rg = "", string endereco = "")
        {
            object result = null;

            data_nasc = Utils.formatarDataZero(data_nasc);
            cpf = Utils.SomenteNumeros(cpf);
            rg = Utils.SomenteNumeros(rg);

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(sexo) && sexo != "M" && sexo != "F")
                    {
                        return ("", new { Erro = true, Mensagem = "Sexo deve ser M ou F" });
                    }

                    if (string.IsNullOrWhiteSpace(cpf) || string.IsNullOrWhiteSpace(nome))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: CPF e NOME." });
                    }

                    if (!Utils.IsCpf(cpf))
                    {
                        return ("", new { Erro = true, Mensagem = "CPF inválido." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        // Novo grupo sempre inicia como Inativo
                        var query = "insert into rift.pessoa_fisica (cpf, nome, sexo, data_nasc, rg, endereco) " +
                                    "values (@cpf, @nome, @sexo, str_to_date(@data_nasc, '%d/%m/%Y'), @rg, @endereco)";

                        dyParam.Add("@cpf", cpf, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@nome", nome, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@sexo", sexo, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@data_nasc", data_nasc, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@rg", rg, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@endereco", endereco, DbType.AnsiString, ParameterDirection.Input);

                        SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        query = "select last_insert_id() id";
                        var v_id = SqlMapper.QuerySingle(conn, query, null, commandType: CommandType.Text);

                        result = new { ID = v_id.id };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Excluir_pessoa_fisica(int id = 0)
        {
            object result = null;
            IDbTransaction transaction = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        transaction = conn.BeginTransaction();

                        var query = "delete from rift.emails_pessoa_fisica where id_pessoa_fisica = @id ";
                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        query = "delete from rift.fones_pessoa_fisica where id_pessoa_fisica = @id ";
                        linhasafetadas = linhasafetadas + SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        query = "delete from rift.pessoa_fisica where id = @id ";
                        linhasafetadas = linhasafetadas + SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        transaction.Commit();

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Atualizar_pessoa_fisica(int id = 0, string cpf = "", string nome = "", string sexo = "", string data_nasc = "", string rg = "", string endereco = "")
        {
            object result = null;

            data_nasc = Utils.formatarDataZero(data_nasc);
            cpf = Utils.SomenteNumeros(cpf);
            rg = Utils.SomenteNumeros(rg);

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {
                    if (id == 0 || string.IsNullOrWhiteSpace(cpf) || string.IsNullOrWhiteSpace(nome) || string.IsNullOrWhiteSpace(sexo)
                        || string.IsNullOrWhiteSpace(data_nasc) || string.IsNullOrWhiteSpace(rg) || string.IsNullOrWhiteSpace(endereco))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID, NOME, SEXO, DATA_NASC, RG, ENDERECO" });
                    }

                    if (!string.IsNullOrWhiteSpace(sexo) && sexo != "M" && sexo != "F")
                    {
                        return ("", new { Erro = true, Mensagem = "Sexo deve ser M ou F" });
                    }

                    if (!Utils.IsCpf(cpf))
                    {
                        return ("", new { Erro = true, Mensagem = "CPF inválido." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "update rift.pessoa_fisica set cpf = @cpf, nome = @nome, sexo = @sexo, data_nasc = str_to_date(@data_nasc, '%d/%m/%Y'), rg = @rg, endereco = @endereco  where id  = @id ";

                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        dyParam.Add("@cpf", cpf, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@nome", nome, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@sexo", sexo, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@data_nasc", data_nasc, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@rg", rg, DbType.AnsiString, ParameterDirection.Input);
                        dyParam.Add("@endereco", endereco, DbType.AnsiString, ParameterDirection.Input);

                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Incluir_email_pf(int id_pessoa_fisica = 0, string email = "")
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {
                try
                {
                   
                    if (id_pessoa_fisica == 0|| string.IsNullOrWhiteSpace(email))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID_PESSOA_FISICA e EMAIL." });
                    }

                    if (!Utils.ValidarEmail(email))
                    {
                        return ("", new { Erro = true, Mensagem = "E-mail inválido." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        // Novo grupo sempre inicia como Inativo
                        var query = "insert into rift.emails_pessoa_fisica (id_pessoa_fisica, email) " +
                                    "values (@id_pessoa_fisica, @email)";

                        dyParam.Add("@id_pessoa_fisica", id_pessoa_fisica, DbType.Int32, ParameterDirection.Input);
                        dyParam.Add("@email", email, DbType.AnsiString, ParameterDirection.Input);

                        SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        query = "select last_insert_id() id";
                        var v_id = SqlMapper.QuerySingle(conn, query, null, commandType: CommandType.Text);

                        result = new { ID = v_id.id };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Incluir_fone_pf(int id_pessoa_fisica = 0, string fone = "")
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {
                try
                {

                    if (id_pessoa_fisica == 0 || string.IsNullOrWhiteSpace(fone))
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID_PESSOA_FISICA e FONE." });
                    }


                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        // Novo grupo sempre inicia como Inativo
                        var query = "insert into rift.fones_pessoa_fisica (id_pessoa_fisica, fone) " +
                                    "values (@id_pessoa_fisica, @fone)";

                        dyParam.Add("@id_pessoa_fisica", id_pessoa_fisica, DbType.Int32, ParameterDirection.Input);
                        dyParam.Add("@fone", fone, DbType.AnsiString, ParameterDirection.Input);

                        SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        query = "select last_insert_id() id";
                        var v_id = SqlMapper.QuerySingle(conn, query, null, commandType: CommandType.Text);

                        result = new { ID = v_id.id };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }


        public object Excluir_email_pf(int id = 0)
        {
            object result = null;
            IDbTransaction transaction = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        transaction = conn.BeginTransaction();

                        var query = "delete from rift.emails_pessoa_fisica where id = @id ";
                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        transaction.Commit();

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Excluir_fone_pf(int id = 0)
        {
            object result = null;
            IDbTransaction transaction = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID." });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        transaction = conn.BeginTransaction();

                        var query = "delete from rift.fones_pessoa_fisica where id = @id ";
                        dyParam.Add("@id", id, DbType.Int32, ParameterDirection.Input);
                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        transaction.Commit();

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }


        public object Consultar_fones_pessoa_fisica(int id_pessoa_fisica = 0)
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id_pessoa_fisica == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar ao menos um parâmetro: ID_PESSOA_FISICA" });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select f.id, f.id_pessoa_fisica, f.fone " +
                                     "from rift.fones_pessoa_fisica f " +
                                     "where f.id_pessoa_fisica = @id_pessoa_fisica";

                        dyParam.Add("@id_pessoa_fisica", id_pessoa_fisica, DbType.Int32, ParameterDirection.Input);

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Consultar_emails_pessoa_fisica(int id_pessoa_fisica = 0)
        {
            object result = null;

            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id_pessoa_fisica == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar ao menos um parâmetro: ID_PESSOA_FISICA" });
                    }

                    var dyParam = new DynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select e.id, e.id_pessoa_fisica, e.email " +
                                    "from rift.emails_pessoa_fisica e " +
                                    "where e.id_pessoa_fisica = @id_pessoa_fisica";

                        dyParam.Add("@id_pessoa_fisica", id_pessoa_fisica, DbType.Int32, ParameterDirection.Input);

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

    }
}
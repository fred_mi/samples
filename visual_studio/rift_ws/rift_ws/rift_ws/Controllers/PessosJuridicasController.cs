﻿using rift_ws.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace rift_ws.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class PessoasJuridicasController : Controller
    {
        PessoasJuridicasRepository pessoasjuridicasRepository;
        ValidarTokenPermissao validarTokenPermissao;

        public PessoasJuridicasController(PessoasJuridicasRepository _pessoasjuridicasRepository)
        {
            pessoasjuridicasRepository = _pessoasjuridicasRepository;
            validarTokenPermissao = new ValidarTokenPermissao(_pessoasjuridicasRepository.GetConfiguration());
        }


        [Route("consultar_pessoa_juridica")]
        [HttpPost]
        public ActionResult Consultar_pessoa_juridica(int id = 0, string razao_social = "", string cnpj = "", string fantasia = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Consultar_pessoa_juridica(id, razao_social, cnpj, fantasia);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("incluir_pessoa_juridica")]
        [HttpPost]
        public ActionResult Incluir_pessoa_juridica(string cnpj = "", string razao_social = "", string fantasia = "", string endereco = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Incluir_pessoa_juridica(cnpj, razao_social, fantasia, endereco);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("incluir_email_pj")]
        [HttpPost]
        public ActionResult Incluir_email_pj(int id_pessoa_juridica = 0, string email = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Incluir_email_pj(id_pessoa_juridica, email);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("incluir_fone_pj")]
        [HttpPost]
        public ActionResult Incluir_fone_pj(int id_pessoa_juridica = 0, string fone = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Incluir_fone_pj(id_pessoa_juridica, fone);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("excluir_pessoa_juridica")]
        [HttpPost]
        public ActionResult Excluir_pessoa_juridica(int id = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Excluir_pessoa_juridica(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("excluir_email_pj")]
        [HttpPost]
        public ActionResult Excluir_email_pj(int id = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Excluir_email_pj(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("excluir_fone_pj")]
        [HttpPost]
        public ActionResult Excluir_fone_pj(int id = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Excluir_fone_pj(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("atualizar_pessoa_juridica")]
        [HttpPost]
        public ActionResult Atualizar_pessoa_juridica(int id = 0, string cnpj = "", string razao_social = "", string fantasia = "", string endereco = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Atualizar_pessoa_juridica(id, cnpj, razao_social, fantasia, endereco);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }


        [Route("consultar_fones_pessoa_juridica")]
        [HttpPost]
        public ActionResult Consultar_fones_pessoa_juridica(int id_pessoa_juridica = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Consultar_fones_pessoa_juridica(id_pessoa_juridica);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("consultar_emails_pessoa_juridica")]
        [HttpPost]
        public ActionResult Consultar_emails_pessoa_juridica(int id_pessoa_juridica = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasjuridicasRepository.Consultar_emails_pessoa_juridica(id_pessoa_juridica);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

    }
}
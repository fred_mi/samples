﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace rift_ws.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class EchoController : Controller
    {
        IConfiguration configuration;
        public EchoController(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        [Route("echo")]
        [HttpPost]
        public ActionResult Echo(string texto = "")
        {
            return Ok(new { echo = texto });
        }
    }
}
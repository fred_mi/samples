﻿using rift_ws.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace rift_ws.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class PessoasFisicasController : Controller
    {
        PessoasFisicasRepository pessoasfisicasRepository;
        ValidarTokenPermissao validarTokenPermissao;

        public PessoasFisicasController(PessoasFisicasRepository _pessoasfisicasRepository)
        {
            pessoasfisicasRepository = _pessoasfisicasRepository;
            validarTokenPermissao = new ValidarTokenPermissao(_pessoasfisicasRepository.GetConfiguration());
        }


        [Route("consultar_pessoa_fisica")]
        [HttpPost]
        public ActionResult Consultar_pessoa_fisica(int id = 0, string nome = "", string cpf = "", string rg = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Consultar_pessoa_fisica(id, nome, cpf, rg);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }


        [Route("incluir_pessoa_fisica")]
        [HttpPost]
        public ActionResult Incluir_pessoa_fisica(string cpf = "", string nome = "", string sexo = "", string data_nasc = "", string rg = "", string endereco = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Incluir_pessoa_fisica(cpf, nome, sexo, data_nasc, rg, endereco);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("incluir_email_pf")]
        [HttpPost]
        public ActionResult Incluir_email_pf(int id_pessoa_fisica = 0, string email = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Incluir_email_pf(id_pessoa_fisica, email);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("incluir_fone_pf")]
        [HttpPost]
        public ActionResult Incluir_fone_pf(int id_pessoa_fisica = 0, string fone = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Incluir_fone_pf(id_pessoa_fisica, fone);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("excluir_pessoa_fisica")]
        [HttpPost]
        public ActionResult Excluir_pessoa_fisica(int id = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Excluir_pessoa_fisica(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("excluir_email_pf")]
        [HttpPost]
        public ActionResult Excluir_email_pf(int id = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Excluir_email_pf(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("excluir_fone_pf")]
        [HttpPost]
        public ActionResult Excluir_fone_pf(int id = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Excluir_fone_pf(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("atualizar_pessoa_fisica")]
        [HttpPost]
        public ActionResult Atualizar_pessoa_fisica(int id = 0, string cpf = "", string nome = "", string sexo = "", string data_nasc = "", string rg = "", string endereco = "", string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Atualizar_pessoa_fisica(id, cpf, nome, sexo, data_nasc, rg, endereco);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }


        [Route("consultar_fones_pessoa_fisica")]
        [HttpPost]
        public ActionResult Consultar_fones_pessoa_fisica(int id_pessoa_fisica = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Consultar_fones_pessoa_fisica(id_pessoa_fisica);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Route("consultar_emails_pessoa_fisica")]
        [HttpPost]
        public ActionResult Consultar_emails_pessoa_fisica(int id_pessoa_fisica = 0, string token = "")
        {
            if (!validarTokenPermissao.Validar(token))
            {
                return Unauthorized();
            }

            var result = pessoasfisicasRepository.Consultar_emails_pessoa_fisica(id_pessoa_fisica);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }



    }
}
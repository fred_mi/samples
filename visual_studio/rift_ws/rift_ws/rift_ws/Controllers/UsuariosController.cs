﻿using rift_ws.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace rift_ws.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class UsuariosController : Controller
    {
        UsuariosRepository usuariosRepository;

        public UsuariosController(UsuariosRepository _usuariosRepository)
        {
            usuariosRepository = _usuariosRepository;
        }

        [Route("login_usuario")]
        [HttpPost]
        public ActionResult Login_usuario(string login = "", string senha = "")
        {
            var result = usuariosRepository.Login_usuario(login, senha);

            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }


    }
}
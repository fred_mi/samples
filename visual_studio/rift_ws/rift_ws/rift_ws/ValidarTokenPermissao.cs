﻿using Dapper;
using MySql.Data.MySqlClient;
using rift_ws.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;

namespace rift_ws
{
    public class ValidarTokenPermissao
    {

        IConfiguration configuration;
        public ValidarTokenPermissao(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public bool Validar(string token = "")
        {
            int id_usuario_login = 0;

            bool token_permissao_ok = false;
    
            using (MySqlConnection conn = new MySqlConnection(Constantes.string_conexao))
            {

                try
                {
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        return false;
                    }

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {

                        var query = "select u.id from rift.usuario u " +
                                    "where u.val_token >= now() " +
                                    "and u.token = @token";

                        var dyParam = new DynamicParameters();
                        dyParam.Add("@token", token, DbType.AnsiString, ParameterDirection.Input);
                       
                        var v_valido = SqlMapper.QuerySingle(conn, query, param: dyParam, commandType: CommandType.Text);
                        token_permissao_ok = v_valido.id != null;
                        id_usuario_login = token_permissao_ok ? unchecked((int)v_valido.id) : 0;

                    }

                    if (token_permissao_ok)
                    {
                        var query = "update rift.usuario set val_token = date_add(now(), interval 30 minute) where id = @id";

                        var dyParam = new DynamicParameters();
                        dyParam.Add("@id", id_usuario_login, DbType.Int32, ParameterDirection.Input);

                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        return true;

                    }
                    else
                    {
                        return false;
                    }

                }
                catch (Exception)
                {
                    return false;
                }

            }
        }
    }
}

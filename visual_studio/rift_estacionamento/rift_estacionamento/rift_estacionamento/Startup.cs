﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using rift_estacionamento.Models;

namespace rift_estacionamento
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(Constantes.RIFT_ESTACIONAMENTO)
                .AddCookie(Constantes.RIFT_ESTACIONAMENTO,
           options =>
           {
               options.LoginPath = new PathString("/auth/login");
               options.AccessDeniedPath = new PathString("/auth/denied");
           });

            services.AddMvc();
            services.AddDistributedMemoryCache();

            services.AddSession();
            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            ReClienteHttp httpClient = ReClienteHttp.Instance;
            httpClient.client.BaseAddress = new Uri(Configuration.GetSection("Uri").GetSection("Uri").Value);
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            string _basepath = "/rift_estacionamento";
            app.UsePathBase(new PathString(_basepath));
            app.Use((context, next) =>
            {
                context.Request.PathBase = _basepath;
                return next();
            });

            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Auth}/{action=Login}/{id?}");
            });
        }
    }
}

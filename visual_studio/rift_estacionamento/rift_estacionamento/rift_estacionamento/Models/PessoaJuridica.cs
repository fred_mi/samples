﻿using System;
using System.Collections.Generic;

namespace rift_estacionamento.Models
{
    public class PessoaJuridica : Base
    {
        public class Dados
        {
            private string _Cnpj;

            public int Id { get; set; }
            public string Cnpj
            {
                get { return _Cnpj; }
                set { _Cnpj = Utils.FormatCNPJ(value); }
            }
            public string Razao_social { get; set; }
            public string Fantasia { get; set; }
            public string Endereco { get; set; }
            public string Fones { get; set; }
            public string Emails { get; set; }
        }

        public List<Dados> Item1 { get; set; }

    }
}
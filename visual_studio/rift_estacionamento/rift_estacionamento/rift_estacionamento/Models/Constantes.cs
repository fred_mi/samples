﻿namespace rift_estacionamento.Models
{
    public static class Constantes
    {
        //Sistema
        public const string NAO = "N";
        public const string SIM = "S";
       
        //Cookies
        public const string RIFT_ESTACIONAMENTO = "rift_est_estacionamento";
        public const string TOKEN_USUARIO = "rift_est_token_usuario";
        public const string LOGADO = "rift_est_logado";
        public const string ID_USUARIO = "rift_est_id_usuario";
        public const string ULTIMA_CONSULTA = "rift_est_ultima_consulta";
        public const string ULTIMA_CONSULTA_AUX = "rift_est_ultima_consulta_aux";
    }
}


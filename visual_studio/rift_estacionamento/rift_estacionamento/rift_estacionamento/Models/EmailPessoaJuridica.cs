﻿using System.Collections.Generic;

namespace rift_estacionamento.Models
{
    public class EmailPessoaJuridica: Base
    {
        public class Dados
        {
            public int Id { get; set; }
            public int Id_pessoa_juridica { get; set; }
            public string Email { get; set; }
        }

        public List<Dados> Item1 { get; set; }

    }
}
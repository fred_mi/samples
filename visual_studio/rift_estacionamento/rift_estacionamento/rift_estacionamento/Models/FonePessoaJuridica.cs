﻿using System.Collections.Generic;

namespace rift_estacionamento.Models
{
    public class FonePessoaJuridica: Base
    {
        public class Dados
        {
            public int Id { get; set; }
            public int Id_pessoa_juridica { get; set; }
            public string Fone { get; set; }
        }

        public List<Dados> Item1 { get; set; }

    }
}
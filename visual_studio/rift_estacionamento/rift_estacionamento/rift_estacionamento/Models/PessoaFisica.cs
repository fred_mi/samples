﻿using System;
using System.Collections.Generic;

namespace rift_estacionamento.Models
{
    public class PessoaFisica : Base
    {
        public class Dados
        {
            private string _Cpf;

            public int Id { get; set; }
            public string Cpf
            {
                get { return _Cpf; }
                set { _Cpf = Utils.FormatCPF(value); }
            }
            public string Nome { get; set; }
            public string Sexo { get; set; }
            public DateTime Data_nasc { get; set; }
            public string Rg { get; set; }
            public string Endereco { get; set; }
            public string Fones { get; set; }
            public string Emails { get; set; }
        }

        public List<Dados> Item1 { get; set; }

    }
}
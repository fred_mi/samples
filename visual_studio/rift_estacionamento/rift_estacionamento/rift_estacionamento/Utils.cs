﻿using rift_estacionamento.Models;
using Microsoft.AspNetCore.Http;
using System;

namespace rift_estacionamento
{
    public class Utils
    {

       public static void formataCabecalho(dynamic ViewBag, HttpRequest Request)
        {
            ViewBag.logado = !string.IsNullOrWhiteSpace(Request.Cookies[Constantes.TOKEN_USUARIO]);
        }

        public static void SetCookie(string cookie, string valor, HttpContext Context)
        {
            if (!string.IsNullOrWhiteSpace(Context.Request.Cookies[cookie]))
            {
                Context.Response.Cookies.Delete(cookie);
            }
            Context.Response.Cookies.Append(cookie, valor);
        }

        public static void SetCookie(string cookie, int valor, HttpContext Context)
        {
            string _valor = valor.ToString();
            if (!string.IsNullOrWhiteSpace(Context.Request.Cookies[cookie]))
            {
                Context.Response.Cookies.Delete(cookie);
            }
            Context.Response.Cookies.Append(cookie, _valor);
        }

        public static void ClearAllCookies(HttpContext Context)
        {
            foreach (var cookie in Context.Request.Cookies.Keys)
            {
                if (cookie.Contains("rift_est_"))
                {
                    Context.Response.Cookies.Delete(cookie);
                }
            }
        }

        public static string FormatCPF(string sender)
        {
            string response = sender.Trim();
            if (response.Length == 11)
            {
                response = response.Insert(9, "-");
                response = response.Insert(6, ".");
                response = response.Insert(3, ".");
            }
            return response;
        }

        public static string FormatCNPJ(string sender)
        {
            string response = sender.Trim();
            if (response.Length == 14)
            {
                response = response.Insert(12, "-");
                response = response.Insert(8, "/");
                response = response.Insert(5, ".");
                response = response.Insert(2, ".");
            }
            return response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using rift_estacionamento.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace rift_estacionamento.Controllers
{
   

    public class AuthController : Controller
    {
        private static string mensagem = "";

        public IActionResult Login()
        {
            ViewData["Message"] = mensagem;
            return View();
        }
        
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string login, string senha)
        {
            mensagem = "";

            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var usuario_senha = new Dictionary<string, string>
            {
                { "login", login},
                { "senha", senha }
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(usuario_senha);

            var response = new HttpResponseMessage();
            try
            {
                response = httpClient.client.PostAsync("api/login_usuario", request.Content).Result;
            }
            catch (Exception e)
            {
                mensagem = "Erro crítico. " + e.Message;
                return RedirectToAction(nameof(Login));
            }

            string data = await response.Content.ReadAsStringAsync();

            Token token = new Token();
            if (response.IsSuccessStatusCode)
            {
                token = JsonConvert.DeserializeObject<Token>(data);
            }
            else
            {
                mensagem = "Erro crítico." + response.StatusCode.ToString();
                return RedirectToAction(nameof(Login));
            }

            if (!token.Item2.Erro && token.Item1.Token != "")
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, login, ClaimValueTypes.String)
                };

                var userIdentity = new ClaimsIdentity(claims, Constantes.RIFT_ESTACIONAMENTO + "SecureLogin");
                var userPrincipal = new ClaimsPrincipal(userIdentity);

                await HttpContext.SignInAsync(Constantes.RIFT_ESTACIONAMENTO,
                    userPrincipal,
                    new AuthenticationProperties
                    {
                        IsPersistent = false,
                        AllowRefresh = false
                    });

                Utils.ClearAllCookies(HttpContext);

                Utils.SetCookie(Constantes.LOGADO, "true", HttpContext);
                Utils.SetCookie(Constantes.TOKEN_USUARIO, token.Item1.Token, HttpContext);
                Utils.SetCookie(Constantes.ID_USUARIO, token.Item1.Id, HttpContext);

                return RedirectToAction("Index", "Home");
            }

            mensagem = token.Item2.Mensagem;

            return RedirectToAction(nameof(Login));

        }

        public async Task<IActionResult> Logout()
        {
            Utils.ClearAllCookies(HttpContext);
            await HttpContext.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }
    }
}
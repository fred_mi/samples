﻿using rift_estacionamento.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;

namespace rift_estacionamento.Controllers
{
    [Authorize]
    public class PessoasJuridicasController : Controller
    {
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult FonesPessoaJuridica(int id_pessoa_juridica, string razao_social, string responseStr, string responseStrFonePJ, string responseStatusCode)
        {

            FonePessoaJuridica _fones_pessoa_juridica = new FonePessoaJuridica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                _fones_pessoa_juridica = JsonConvert.DeserializeObject<FonePessoaJuridica>(responseStr, settings);
            }

            ProcessarRegistro _processarregistro = new ProcessarRegistro();

            if (!string.IsNullOrWhiteSpace(responseStrFonePJ))
            {
                _processarregistro = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStrFonePJ);
            }


            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id_pessoa_juridica = id_pessoa_juridica;
            ViewBag.razao_social = razao_social;

            ViewBag.fones_pj = (_fones_pessoa_juridica == null || _fones_pessoa_juridica.Item1 == null) ? new List<FonePessoaJuridica.Dados>() : _fones_pessoa_juridica.Item1;

            ViewBag.erro = (_processarregistro == null || _processarregistro.Item2 == null ? false : _processarregistro.Item2.Erro);
            ViewBag.linhasafetadas = (_processarregistro == null || _processarregistro.Item1 == null ? -1 : _processarregistro.Item1.Linhasafetadas);
            ViewBag.mensagem = (_processarregistro == null || _processarregistro.Item2 == null ? "" : _processarregistro.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            return View("FonesPessoaJuridica");
        }

        public IActionResult EmailsPessoaJuridica(int id_pessoa_juridica, string razao_social, string responseStr, string responseStrEmailPJ, string responseStatusCode)
        {

            EmailPessoaJuridica _emails_pessoa_juridica = new EmailPessoaJuridica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                _emails_pessoa_juridica = JsonConvert.DeserializeObject<EmailPessoaJuridica>(responseStr, settings);
            }

            ProcessarRegistro _processarregistro = new ProcessarRegistro();

            if (!string.IsNullOrWhiteSpace(responseStrEmailPJ))
            {
                _processarregistro = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStrEmailPJ);
            }


            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id_pessoa_juridica = id_pessoa_juridica;
            ViewBag.razao_social = razao_social;

            ViewBag.emails_pj = (_emails_pessoa_juridica == null || _emails_pessoa_juridica.Item1 == null) ? new List<EmailPessoaJuridica.Dados>() : _emails_pessoa_juridica.Item1;

            ViewBag.erro = (_processarregistro == null || _processarregistro.Item2 == null ? false : _processarregistro.Item2.Erro);
            ViewBag.linhasafetadas = (_processarregistro == null || _processarregistro.Item1 == null ? -1 : _processarregistro.Item1.Linhasafetadas);
            ViewBag.mensagem = (_processarregistro == null || _processarregistro.Item2 == null ? "" : _processarregistro.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            return View("EmailsPessoaJuridica");
        }

        public IActionResult EditarPessoasJuridicas(int id, string responseStr, string responseStatusCode)
        {

            ProcessarRegistro _processarregistro = new ProcessarRegistro();
            PessoaJuridica _pessoajuridica = new PessoaJuridica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                if (id == 0)
                {
                    _processarregistro = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStr);
                }
                else
                {
                    _pessoajuridica = JsonConvert.DeserializeObject<PessoaJuridica>(responseStr);
                }
            }


            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id = id;
            ViewBag.cnpj = (_pessoajuridica == null || _pessoajuridica.Item1 == null) ? "" : _pessoajuridica.Item1[0].Cnpj;
            ViewBag.razao_social = (_pessoajuridica == null || _pessoajuridica.Item1 == null) ? "" : _pessoajuridica.Item1[0].Razao_social;
            ViewBag.fantasia = (_pessoajuridica == null || _pessoajuridica.Item1 == null) ? "" : _pessoajuridica.Item1[0].Fantasia;
            ViewBag.endereco = (_pessoajuridica == null || _pessoajuridica.Item1 == null) ? "" : _pessoajuridica.Item1[0].Endereco;

            ViewBag.erro = (_processarregistro == null || _processarregistro.Item2 == null ? false : _processarregistro.Item2.Erro);
            ViewBag.linhasafetadas = (_processarregistro == null || _processarregistro.Item1 == null ? -1 : _processarregistro.Item1.Linhasafetadas);
            ViewBag.mensagem = (_processarregistro == null || _processarregistro.Item2 == null ? "" : _processarregistro.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            return View("EditarPessoasJuridicas");
        }


        public IActionResult IncluirPessoasJuridicas(string responseStr, string responseStatusCode, string inclusao_pessoajuridica)
        {
            NovoRegistro _novapessofisica = new NovoRegistro();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                _novapessofisica = JsonConvert.DeserializeObject<NovoRegistro>(responseStr);
            }

            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id = (_novapessofisica == null || _novapessofisica.Item1 == null ? 0 : _novapessofisica.Item1.Id);
            ViewBag.erro = (_novapessofisica == null  || _novapessofisica.Item2 == null ? false : _novapessofisica.Item2.Erro);
            ViewBag.mensagem = (_novapessofisica == null || _novapessofisica.Item2 == null ? "" : _novapessofisica.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            if (ViewBag.erro)
            {
                Dictionary<string, string> inclusao = JsonConvert.DeserializeObject<Dictionary<string, string>>(inclusao_pessoajuridica);
                ViewBag.cnpj = inclusao["cnpj"];
                ViewBag.razao_social = inclusao["razao_social"];
                ViewBag.fantasia = inclusao["fantasia"];
                ViewBag.endereco = inclusao["endereco"];
            }

            return View("IncluirPessoasJuridicas");
        }

        public IActionResult ConsultarPessoasJuridicas(string responseStr, string responseStrExc, string responseStatusCode)
        {
            PessoaJuridica _pessoajuridica = new PessoaJuridica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                _pessoajuridica = JsonConvert.DeserializeObject<PessoaJuridica>(responseStr);
                ViewBag.erro = (_pessoajuridica == null || _pessoajuridica.Item2 == null ? false : _pessoajuridica.Item2.Erro);
                ViewBag.mensagem = (_pessoajuridica == null || _pessoajuridica.Item2 == null ? "" : _pessoajuridica.Item2.Mensagem);
                ViewBag.excluindo = false;
            }

            if (!string.IsNullOrWhiteSpace(responseStrExc))
            {
                ProcessarRegistro _proc = new ProcessarRegistro();
                _proc = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStrExc);
                ViewBag.erro = (_proc == null || _proc.Item2 == null ? false : _proc.Item2.Erro);
                ViewBag.mensagem = (_proc == null || _proc.Item2 == null ? "" : _proc.Item2.Mensagem);
                ViewBag.excluindo = true;
            }

          
            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.pessoasjuridicas = (_pessoajuridica == null || _pessoajuridica.Item1 == null) ? new List<PessoaJuridica.Dados>() : _pessoajuridica.Item1;

            ViewBag.responseStatusCode = responseStatusCode;

            return View("ConsultarPessoasJuridicas");
        }


        [HttpPost]
        public async Task<IActionResult>IncluirPessoasJuridicasPost( string cnpj, string razao_social, string fantasia, string endereco)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var novo_pessoajuridica = new Dictionary<string, string>
            {
                { "cnpj", cnpj },
                { "razao_social", razao_social },
                { "fantasia", fantasia },
                { "endereco", endereco },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(novo_pessoajuridica);

            var response = httpClient.client.PostAsync("api/incluir_pessoa_juridica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return IncluirPessoasJuridicas(responseStr, response.StatusCode.ToString(), JsonConvert.SerializeObject(novo_pessoajuridica));
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
           
        }

        public Task<IActionResult> RefazerUltimaConsultaPessoaJuridica()
        {
           return ConsultarPessoasJuridicasIn(true);
        }

        [HttpPost]
        public async Task<IActionResult> ConsultarPessoasJuridicasPost( int id, string razao_social, string cnpj, string fantasia)
        {
            return await ConsultarPessoasJuridicasIn(false, false, id, razao_social, cnpj, fantasia);
        }

        private async Task<IActionResult> ConsultarPessoasJuridicasIn(bool refazerultimaconsulta = false, bool editando = false, int id = 0, string razao_social = "", string cnpj = "", string fantasia = "")
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var consulta_pessoajuridica = new Dictionary<string, string>
            {
                { "id", id.ToString() },
                { "cnpj", cnpj },
                { "razao_social", razao_social },
                { "fantasia", fantasia },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
            };

            
            if (refazerultimaconsulta)
            {
                consulta_pessoajuridica = JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Cookies[Constantes.ULTIMA_CONSULTA]);
            }

            
            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(consulta_pessoajuridica);

            var response = httpClient.client.PostAsync("api/consultar_pessoa_juridica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                if (!editando)
                {
                    Utils.SetCookie(Constantes.ULTIMA_CONSULTA, JsonConvert.SerializeObject(consulta_pessoajuridica), HttpContext);
                    return ConsultarPessoasJuridicas(responseStr, "", response.StatusCode.ToString());
                }
                else
                {
                    return EditarPessoasJuridicas(id, responseStr, response.StatusCode.ToString());
                }
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public async Task<IActionResult> ExcluirPessoasJuridicas(int id)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var excluir_pessoajuridica = new Dictionary<string, string>
          {
              { "id", id.ToString() },
              { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
          };
            
            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(excluir_pessoajuridica);

            var response = httpClient.client.PostAsync("api/excluir_pessoa_juridica", request.Content).Result;

            string responseStrExc = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return ConsultarPessoasJuridicas("", responseStrExc, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public Task<IActionResult> AcaoEditarPessoasJuridicas(int id)
        {
            return ConsultarPessoasJuridicasIn(false, true, id);
        }


        [HttpPost]
        public async Task<IActionResult> EditarPessoasJuridicasPost( int id, string cnpj, string razao_social, string fantasia, string endereco)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var edt_pessoajuridica = new Dictionary<string, string>
            {
                { "id", id.ToString() },
                { "cnpj", cnpj },
                { "razao_social", razao_social },
                { "fantasia", fantasia },
                { "endereco", endereco },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(edt_pessoajuridica);

            var response = httpClient.client.PostAsync("api/atualizar_pessoa_juridica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return EditarPessoasJuridicas(0, responseStr, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }

        }

        public Task<IActionResult> AcaoFonesPessoaJuridica(int id_pessoa_juridica, string razao_social, string responseStrFonePJ)
        {
            return ConsultarFonesPessoaJuridicaIn(false, id_pessoa_juridica, razao_social, responseStrFonePJ);
        }

        private async Task<IActionResult> ConsultarFonesPessoaJuridicaIn(bool refazerultimaconsulta = false, int id_pessoa_juridica = 0, string razao_social = "", string responseStrFonePJ = "")
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var consulta_fone_pessoa_juridica = new Dictionary<string, string>
            {
                { "id_pessoa_juridica", id_pessoa_juridica.ToString() },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };


            if (refazerultimaconsulta)
            {
                consulta_fone_pessoa_juridica = JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Cookies[Constantes.ULTIMA_CONSULTA_AUX]);
            }

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(consulta_fone_pessoa_juridica);

            var response = httpClient.client.PostAsync("api/consultar_fones_pessoa_juridica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            Utils.SetCookie(Constantes.ULTIMA_CONSULTA_AUX, JsonConvert.SerializeObject(consulta_fone_pessoa_juridica), HttpContext);

            if (response.IsSuccessStatusCode)
            {
                return FonesPessoaJuridica(id_pessoa_juridica, razao_social, responseStr, responseStrFonePJ, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }


        }

        public async Task<IActionResult> ExcluirFonePessoaJuridica(int id, int id_pessoa_juridica, string razao_social)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var excluir_fone_pj = new Dictionary<string, string>
          {
              { "id", id.ToString() },
              { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
          };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(excluir_fone_pj);

            var response = httpClient.client.PostAsync("api/excluir_fone_pj", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarFonesPessoaJuridicaIn(true, id_pessoa_juridica, razao_social, responseStr);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public async Task<IActionResult> IncluirFonesPessoaJuridicaPost(int id_pessoa_juridica, string fone, string razao_social)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var novo_grppermusr = new Dictionary<string, string>
            {
                { "id_pessoa_juridica", id_pessoa_juridica.ToString() },
                { "fone", fone },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(novo_grppermusr);

            var response = httpClient.client.PostAsync("api/incluir_fone_pj", request.Content).Result;

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarFonesPessoaJuridicaIn(true, id_pessoa_juridica, razao_social);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public Task<IActionResult> AcaoEmailsPessoaJuridica(int id_pessoa_juridica, string razao_social, string responseStrFonePJ)
        {
            return ConsultarEmailsPessoaJuridicaIn(false, id_pessoa_juridica, razao_social, responseStrFonePJ);
        }

        private async Task<IActionResult> ConsultarEmailsPessoaJuridicaIn(bool refazerultimaconsulta = false, int id_pessoa_juridica = 0, string razao_social = "", string responseStrFonePJ = "")
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var consulta_email_pessoa_juridica = new Dictionary<string, string>
            {
                { "id_pessoa_juridica", id_pessoa_juridica.ToString() },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };


            if (refazerultimaconsulta)
            {
                consulta_email_pessoa_juridica = JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Cookies[Constantes.ULTIMA_CONSULTA_AUX]);
            }

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(consulta_email_pessoa_juridica);

            var response = httpClient.client.PostAsync("api/consultar_emails_pessoa_juridica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            Utils.SetCookie(Constantes.ULTIMA_CONSULTA_AUX, JsonConvert.SerializeObject(consulta_email_pessoa_juridica), HttpContext);

            if (response.IsSuccessStatusCode)
            {
                return EmailsPessoaJuridica(id_pessoa_juridica, razao_social, responseStr, responseStrFonePJ, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }

        }

        public async Task<IActionResult> ExcluirEmailPessoaJuridica(int id, int id_pessoa_juridica, string razao_social)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var excluir_email_pj = new Dictionary<string, string>
          {
              { "id", id.ToString() },
              { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
          };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(excluir_email_pj);

            var response = httpClient.client.PostAsync("api/excluir_email_pj", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarEmailsPessoaJuridicaIn(true, id_pessoa_juridica, razao_social, responseStr);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public async Task<IActionResult> IncluirEmailsPessoaJuridicaPost(int id_pessoa_juridica, string email, string razao_social)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var novo_grppermusr = new Dictionary<string, string>
            {
                { "id_pessoa_juridica", id_pessoa_juridica.ToString() },
                { "email", email},
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(novo_grppermusr);

            var response = httpClient.client.PostAsync("api/incluir_email_pj", request.Content).Result;

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarEmailsPessoaJuridicaIn(true, id_pessoa_juridica, razao_social);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }


    }
}
﻿using rift_estacionamento.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;

namespace rift_estacionamento.Controllers
{
    [Authorize]
    public class PessoasFisicasController : Controller
    {
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult FonesPessoaFisica(int id_pessoa_fisica, string nome, string responseStr, string responseStrFonePF, string responseStatusCode)
        {

            FonePessoaFisica _fones_pessoa_fisica = new FonePessoaFisica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                _fones_pessoa_fisica = JsonConvert.DeserializeObject<FonePessoaFisica>(responseStr, settings);
            }

            ProcessarRegistro _processarregistro = new ProcessarRegistro();

            if (!string.IsNullOrWhiteSpace(responseStrFonePF))
            {
                _processarregistro = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStrFonePF);
            }


            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id_pessoa_fisica = id_pessoa_fisica;
            ViewBag.nome = nome;

            ViewBag.fones_pf = (_fones_pessoa_fisica == null || _fones_pessoa_fisica.Item1 == null) ? new List<FonePessoaFisica.Dados>() : _fones_pessoa_fisica.Item1;

            ViewBag.erro = (_processarregistro == null || _processarregistro.Item2 == null ? false : _processarregistro.Item2.Erro);
            ViewBag.linhasafetadas = (_processarregistro == null || _processarregistro.Item1 == null ? -1 : _processarregistro.Item1.Linhasafetadas);
            ViewBag.mensagem = (_processarregistro == null || _processarregistro.Item2 == null ? "" : _processarregistro.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            return View("FonesPessoaFisica");
        }

        public IActionResult EmailsPessoaFisica(int id_pessoa_fisica, string nome, string responseStr, string responseStrEmailPF, string responseStatusCode)
        {

            EmailPessoaFisica _emails_pessoa_fisica = new EmailPessoaFisica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                _emails_pessoa_fisica = JsonConvert.DeserializeObject<EmailPessoaFisica>(responseStr, settings);
            }

            ProcessarRegistro _processarregistro = new ProcessarRegistro();

            if (!string.IsNullOrWhiteSpace(responseStrEmailPF))
            {
                _processarregistro = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStrEmailPF);
            }


            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id_pessoa_fisica = id_pessoa_fisica;
            ViewBag.nome = nome;

            ViewBag.emails_pf = (_emails_pessoa_fisica == null || _emails_pessoa_fisica.Item1 == null) ? new List<EmailPessoaFisica.Dados>() : _emails_pessoa_fisica.Item1;

            ViewBag.erro = (_processarregistro == null || _processarregistro.Item2 == null ? false : _processarregistro.Item2.Erro);
            ViewBag.linhasafetadas = (_processarregistro == null || _processarregistro.Item1 == null ? -1 : _processarregistro.Item1.Linhasafetadas);
            ViewBag.mensagem = (_processarregistro == null || _processarregistro.Item2 == null ? "" : _processarregistro.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            return View("EmailsPessoaFisica");
        }

        public IActionResult EditarPessoasFisicas(int id, string responseStr, string responseStatusCode)
        {

            ProcessarRegistro _processarregistro = new ProcessarRegistro();
            PessoaFisica _pessoafisica = new PessoaFisica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                if (id == 0)
                {
                    _processarregistro = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStr);
                }
                else
                {
                    _pessoafisica = JsonConvert.DeserializeObject<PessoaFisica>(responseStr);
                }
            }


            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id = id;
            ViewBag.cpf = (_pessoafisica == null || _pessoafisica.Item1 == null) ? "" : _pessoafisica.Item1[0].Cpf;
            ViewBag.nome = (_pessoafisica == null || _pessoafisica.Item1 == null) ? "" : _pessoafisica.Item1[0].Nome;
            ViewBag.sexo = (_pessoafisica == null || _pessoafisica.Item1 == null) ? "" : _pessoafisica.Item1[0].Sexo;
            ViewBag.data_nasc = (_pessoafisica == null || _pessoafisica.Item1 == null) ? "" : _pessoafisica.Item1[0].Data_nasc.ToString("yyyy-MM-dd");
            ViewBag.rg = (_pessoafisica == null || _pessoafisica.Item1 == null) ? "" : _pessoafisica.Item1[0].Rg;
            ViewBag.endereco = (_pessoafisica == null || _pessoafisica.Item1 == null) ? "" : _pessoafisica.Item1[0].Endereco;

            ViewBag.erro = (_processarregistro == null || _processarregistro.Item2 == null ? false : _processarregistro.Item2.Erro);
            ViewBag.linhasafetadas = (_processarregistro == null || _processarregistro.Item1 == null ? -1 : _processarregistro.Item1.Linhasafetadas);
            ViewBag.mensagem = (_processarregistro == null || _processarregistro.Item2 == null ? "" : _processarregistro.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            return View("EditarPessoasFisicas");
        }


        public IActionResult IncluirPessoasFisicas(string responseStr, string responseStatusCode, string inclusao_pessoafisica)
        {
            NovoRegistro _novapessofisica = new NovoRegistro();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                _novapessofisica = JsonConvert.DeserializeObject<NovoRegistro>(responseStr);
            }

            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.id = (_novapessofisica == null || _novapessofisica.Item1 == null ? 0 : _novapessofisica.Item1.Id);
            ViewBag.erro = (_novapessofisica == null  || _novapessofisica.Item2 == null ? false : _novapessofisica.Item2.Erro);
            ViewBag.mensagem = (_novapessofisica == null || _novapessofisica.Item2 == null ? "" : _novapessofisica.Item2.Mensagem);
            ViewBag.responseStatusCode = responseStatusCode;

            if (ViewBag.erro)
            {
                Dictionary<string, string> inclusao = JsonConvert.DeserializeObject<Dictionary<string, string>>(inclusao_pessoafisica);
                ViewBag.cpf = inclusao["cpf"];
                ViewBag.nome = inclusao["nome"];
                ViewBag.sexo = inclusao["sexo"];
                ViewBag.data_nasc = inclusao["data_nasc"];
                ViewBag.rg = inclusao["rg"];
                ViewBag.endereco = inclusao["endereco"];
            }

            return View("IncluirPessoasFisicas");
        }

        public IActionResult ConsultarPessoasFisicas(string responseStr, string responseStrExc, string responseStatusCode)
        {
            PessoaFisica _pessoafisica = new PessoaFisica();

            if (!string.IsNullOrWhiteSpace(responseStr))
            {
                _pessoafisica = JsonConvert.DeserializeObject<PessoaFisica>(responseStr);
                ViewBag.erro = (_pessoafisica == null || _pessoafisica.Item2 == null ? false : _pessoafisica.Item2.Erro);
                ViewBag.mensagem = (_pessoafisica == null || _pessoafisica.Item2 == null ? "" : _pessoafisica.Item2.Mensagem);
                ViewBag.excluindo = false;
            }

            if (!string.IsNullOrWhiteSpace(responseStrExc))
            {
                ProcessarRegistro _proc = new ProcessarRegistro();
                _proc = JsonConvert.DeserializeObject<ProcessarRegistro>(responseStrExc);
                ViewBag.erro = (_proc == null || _proc.Item2 == null ? false : _proc.Item2.Erro);
                ViewBag.mensagem = (_proc == null || _proc.Item2 == null ? "" : _proc.Item2.Mensagem);
                ViewBag.excluindo = true;
            }

          
            Utils.formataCabecalho(ViewBag, Request);
            ViewBag.pessoasfisicas = (_pessoafisica == null || _pessoafisica.Item1 == null) ? new List<PessoaFisica.Dados>() : _pessoafisica.Item1;

            ViewBag.responseStatusCode = responseStatusCode;

            return View("ConsultarPessoasFisicas");
        }


        [HttpPost]
        public async Task<IActionResult>IncluirPessoasFisicasPost( string cpf, string nome, string sexo, DateTime data_nasc, string rg, string endereco)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var novo_pessoafisica = new Dictionary<string, string>
            {
                { "cpf", cpf },
                { "nome", nome },
                { "sexo", sexo },
                { "data_nasc", data_nasc.ToString("dd/MM/yyyy") },
                { "rg", rg },
                { "endereco", endereco },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(novo_pessoafisica);

            var response = httpClient.client.PostAsync("api/incluir_pessoa_fisica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return IncluirPessoasFisicas(responseStr, response.StatusCode.ToString(), JsonConvert.SerializeObject(novo_pessoafisica));
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
           
        }

        public Task<IActionResult> RefazerUltimaConsultaPessoaFisica()
        {
           return ConsultarPessoasFisicasIn(true);
        }

        [HttpPost]
        public async Task<IActionResult> ConsultarPessoasFisicasPost( int id, string nome, string cpf, string rg)
        {
            return await ConsultarPessoasFisicasIn(false, false, id, nome, cpf, rg);
        }

        private async Task<IActionResult> ConsultarPessoasFisicasIn(bool refazerultimaconsulta = false, bool editando = false, int id = 0, string nome = "", string cpf = "", string rg = "")
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var consulta_pessoafisica = new Dictionary<string, string>
            {
                { "id", id.ToString() },
                { "cpf", cpf },
                { "nome", nome },
                { "rg", rg },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
            };

            
            if (refazerultimaconsulta)
            {
                consulta_pessoafisica = JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Cookies[Constantes.ULTIMA_CONSULTA]);
            }

            
            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(consulta_pessoafisica);

            var response = httpClient.client.PostAsync("api/consultar_pessoa_fisica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                if (!editando)
                {
                    Utils.SetCookie(Constantes.ULTIMA_CONSULTA, JsonConvert.SerializeObject(consulta_pessoafisica), HttpContext);
                    return ConsultarPessoasFisicas(responseStr, "", response.StatusCode.ToString());
                }
                else
                {
                    return EditarPessoasFisicas(id, responseStr, response.StatusCode.ToString());
                }
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public async Task<IActionResult> ExcluirPessoasFisicas(int id)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var excluir_pessoafisica = new Dictionary<string, string>
          {
              { "id", id.ToString() },
              { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
          };
            
            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(excluir_pessoafisica);

            var response = httpClient.client.PostAsync("api/excluir_pessoa_fisica", request.Content).Result;

            string responseStrExc = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return ConsultarPessoasFisicas("", responseStrExc, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public Task<IActionResult> AcaoEditarPessoasFisicas(int id)
        {
            return ConsultarPessoasFisicasIn(false, true, id);
        }


        [HttpPost]
        public async Task<IActionResult> EditarPessoasFisicasPost( int id, string cpf, string nome, string sexo, DateTime data_nasc, string rg, string endereco)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var edt_pessoafisica = new Dictionary<string, string>
            {
                { "id", id.ToString() },
                { "cpf", cpf },
                { "nome", nome },
                { "sexo", sexo },
                { "data_nasc", data_nasc.ToString("dd/MM/yyyy") },
                { "rg", rg },
                { "endereco", endereco },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO] }
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(edt_pessoafisica);

            var response = httpClient.client.PostAsync("api/atualizar_pessoa_fisica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return EditarPessoasFisicas(0, responseStr, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }

        }

        public Task<IActionResult> AcaoFonesPessoaFisica(int id_pessoa_fisica, string nome, string responseStrFonePF)
        {
            return ConsultarFonesPessoaFisicaIn(false, id_pessoa_fisica, nome, responseStrFonePF);
        }

        private async Task<IActionResult> ConsultarFonesPessoaFisicaIn(bool refazerultimaconsulta = false, int id_pessoa_fisica = 0, string nome = "", string responseStrFonePF = "")
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var consulta_fone_pessoa_fisica = new Dictionary<string, string>
            {
                { "id_pessoa_fisica", id_pessoa_fisica.ToString() },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };


            if (refazerultimaconsulta)
            {
                consulta_fone_pessoa_fisica = JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Cookies[Constantes.ULTIMA_CONSULTA_AUX]);
            }

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(consulta_fone_pessoa_fisica);

            var response = httpClient.client.PostAsync("api/consultar_fones_pessoa_fisica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            Utils.SetCookie(Constantes.ULTIMA_CONSULTA_AUX, JsonConvert.SerializeObject(consulta_fone_pessoa_fisica), HttpContext);

            if (response.IsSuccessStatusCode)
            {
                return FonesPessoaFisica(id_pessoa_fisica, nome, responseStr, responseStrFonePF, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }


        }

        public async Task<IActionResult> ExcluirFonePessoaFisica(int id, int id_pessoa_fisica, string nome)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var excluir_fone_pf = new Dictionary<string, string>
          {
              { "id", id.ToString() },
              { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
          };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(excluir_fone_pf);

            var response = httpClient.client.PostAsync("api/excluir_fone_pf", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarFonesPessoaFisicaIn(true, id_pessoa_fisica, nome, responseStr);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public async Task<IActionResult> IncluirFonesPessoaFisicaPost(int id_pessoa_fisica, string fone, string nome)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var novo_grppermusr = new Dictionary<string, string>
            {
                { "id_pessoa_fisica", id_pessoa_fisica.ToString() },
                { "fone", fone },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(novo_grppermusr);

            var response = httpClient.client.PostAsync("api/incluir_fone_pf", request.Content).Result;

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarFonesPessoaFisicaIn(true, id_pessoa_fisica, nome);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public Task<IActionResult> AcaoEmailsPessoaFisica(int id_pessoa_fisica, string nome, string responseStrFonePF)
        {
            return ConsultarEmailsPessoaFisicaIn(false, id_pessoa_fisica, nome, responseStrFonePF);
        }

        private async Task<IActionResult> ConsultarEmailsPessoaFisicaIn(bool refazerultimaconsulta = false, int id_pessoa_fisica = 0, string nome = "", string responseStrFonePF = "")
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var consulta_email_pessoa_fisica = new Dictionary<string, string>
            {
                { "id_pessoa_fisica", id_pessoa_fisica.ToString() },
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };


            if (refazerultimaconsulta)
            {
                consulta_email_pessoa_fisica = JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Cookies[Constantes.ULTIMA_CONSULTA_AUX]);
            }

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(consulta_email_pessoa_fisica);

            var response = httpClient.client.PostAsync("api/consultar_emails_pessoa_fisica", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            Utils.SetCookie(Constantes.ULTIMA_CONSULTA_AUX, JsonConvert.SerializeObject(consulta_email_pessoa_fisica), HttpContext);

            if (response.IsSuccessStatusCode)
            {
                return EmailsPessoaFisica(id_pessoa_fisica, nome, responseStr, responseStrFonePF, response.StatusCode.ToString());
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }

        }

        public async Task<IActionResult> ExcluirEmailPessoaFisica(int id, int id_pessoa_fisica, string nome)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var excluir_email_pf = new Dictionary<string, string>
          {
              { "id", id.ToString() },
              { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
          };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(excluir_email_pf);

            var response = httpClient.client.PostAsync("api/excluir_email_pf", request.Content).Result;

            string responseStr = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarEmailsPessoaFisicaIn(true, id_pessoa_fisica, nome, responseStr);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }

        public async Task<IActionResult> IncluirEmailsPessoaFisicaPost(int id_pessoa_fisica, string email, string nome)
        {
            ReClienteHttp httpClient = ReClienteHttp.Instance;

            var novo_grppermusr = new Dictionary<string, string>
            {
                { "id_pessoa_fisica", id_pessoa_fisica.ToString() },
                { "email", email},
                { "token", Request.Cookies[Constantes.TOKEN_USUARIO]}
            };

            var request = new HttpRequestMessage();
            request.Content = new FormUrlEncodedContent(novo_grppermusr);

            var response = httpClient.client.PostAsync("api/incluir_email_pf", request.Content).Result;

            if (response.IsSuccessStatusCode)
            {
                return await ConsultarEmailsPessoaFisicaIn(true, id_pessoa_fisica, nome);
            }
            else
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                else
                {
                    return RedirectToAction(nameof(Error));
                }
            }
        }


    }
}
﻿using rift_estacionamento.Models;
using Microsoft.AspNetCore.Http;
using System.Net.Http;

namespace rift_estacionamento
{
    public class ReClienteHttp
    {
        private static readonly HttpClient _client = new HttpClient();
        public HttpClient client;

        public ReClienteHttp()
        {
            client = _client;
        }

        public static ReClienteHttp Instance { get; } = new ReClienteHttp();

    }
}

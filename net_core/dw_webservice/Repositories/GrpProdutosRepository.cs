﻿using Dapper;
using dw_webservice.Models;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace dw_webservice.Repositories
{
    public class GrpProdutosRepository 
    {
        IConfiguration configuration;
        public GrpProdutosRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public IConfiguration GetConfiguration()
        {
            return configuration;
        }


        public object Consultar_todos_grp_produto()
        {
            object result = null;

            using (OracleConnection conn = new OracleConnection(Constantes.string_conexao))
            {

                try
                {

                    var dyParam = new OracleDynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "with tipos as ( select id_grp_prod, listagg(tp.descricao, ', ') " +
                            "within group(order by tp.ordem)  as tipos_produtos " +
                            "from tp_produtos tp where tp.id_situacao = :situacao group by id_grp_prod) " +
                            "select gp.id, gp.descricao, tipos_produtos, gp.icone " +
                            "from grp_produtos gp, tipos tp " +
                            "where gp.id = tp.id_grp_prod and gp.id_situacao = :situacao " +
                            "order by gp.ordem, gp.descricao";
                        dyParam.Add("situacao", Constantes.ATIVO, OracleDbType.Int32, ParameterDirection.Input);

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }


        public object Consultar_grp_produto(int id = 0, string descricao = "", int id_situacao = 0)
        {
            object result = null;

            using (OracleConnection conn = new OracleConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0 && string.IsNullOrWhiteSpace(descricao) && id_situacao == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar ao menos um parâmetro: ID, DESCRICAO ou ID_SITUACAO." });
                    }

                    var dyParam = new OracleDynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "select gp.id, gp.descricao, gp.id_situacao, s.descricao desc_situacao, gp.icone, gp.ordem " +
                                    "from grp_produtos gp, situacoes s " +
                                    "where gp.id_situacao = s.id and ( ";
                        var tem_param = false;

                        if (id != 0)
                        {
                            query = query + " gp.id = :id";
                            dyParam.Add("id", id, OracleDbType.Int32, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (!string.IsNullOrWhiteSpace(descricao))
                        {
                            query = query + (tem_param ? " and " : "") + " upper(gp.descricao) like upper(:descricao)";
                            dyParam.Add("descricao", "%" + descricao + "%", OracleDbType.NVarchar2, ParameterDirection.Input);
                            tem_param = true;
                        }

                        if (id_situacao != 0)
                        {
                            query = query + (tem_param ? " and " : "") + " gp.id_situacao = :id_situacao";
                            dyParam.Add("id_situacao", id_situacao, OracleDbType.NVarchar2, ParameterDirection.Input);
                            tem_param = true;
                        }

                        query = query + " ) order by gp.ordem, gp.descricao";

                        result = SqlMapper.Query(conn, query, param: dyParam, commandType: CommandType.Text);

                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Incluir_grp_produto(string descricao = "", string icone = "", int ordem = 0, int id_usuario_login = 0)
        {
            object result = null;

            using (OracleConnection conn = new OracleConnection(Constantes.string_conexao))
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(descricao) || string.IsNullOrWhiteSpace(icone) || ordem == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: DESCRICAO, ICONE e ORDEM." });
                    }

                    var dyParam = new OracleDynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        // Novo grupo sempre inicia como Inativo
                        var query = "insert into grp_produtos (descricao, id_situacao, icone, ordem, id_usuario_log) " +
                                    "values (:descricao, :situacao, :icone, :ordem, :id_usuario_login) returning id into :id ";

                        dyParam.Add("id", null, OracleDbType.Int32, ParameterDirection.Output);
                        dyParam.Add("descricao", descricao, OracleDbType.NVarchar2, ParameterDirection.Input);
                        dyParam.Add("icone", icone, OracleDbType.NVarchar2, ParameterDirection.Input);
                        dyParam.Add("situacao", Constantes.INATIVO, OracleDbType.NVarchar2, ParameterDirection.Input);
                        dyParam.Add("ordem", ordem, OracleDbType.Int32, ParameterDirection.Input);
                        dyParam.Add("id_usuario_login", id_usuario_login, OracleDbType.Int32, ParameterDirection.Input);

                        SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        Utils.Atualizar_valor_config("ultima_atualizacao_produtos", DateTime.Now.ToString());

                        result = new { ID = dyParam.Get<int>("id") };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Excluir_grp_produto(int id = 0, int id_usuario_login = 0)
        {
            object result = null;
            IDbTransaction transaction = null;

            using (OracleConnection conn = new OracleConnection(Constantes.string_conexao))
            {

                try
                {

                    if (id == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID." });
                    }

                    var dyParam = new OracleDynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        transaction = conn.BeginTransaction();

                        var query = "update grp_produtos set id_usuario_log = :id_usuario_login where id = :id ";
                        dyParam.Add("id", id, OracleDbType.Int32, ParameterDirection.Input);
                        dyParam.Add("id_usuario_login", id_usuario_login, OracleDbType.Int32, ParameterDirection.Input);
                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        query = "delete from grp_produtos where id = :id ";

                        linhasafetadas = linhasafetadas + SqlMapper.Execute(conn, query, dyParam, transaction, null, CommandType.Text);

                        Utils.Atualizar_valor_config("ultima_atualizacao_produtos", DateTime.Now.ToString());

                        transaction.Commit();

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Atualizar_grp_produto(int id = 0, string descricao = "", int id_situacao = 0, string icone = "", int ordem = 0, int id_usuario_login = 0)
        {
            object result = null;

            using (OracleConnection conn = new OracleConnection(Constantes.string_conexao))
            {

                try
                {
                    if (id == 0 || string.IsNullOrWhiteSpace(descricao) || string.IsNullOrWhiteSpace(icone) || id_situacao == 0 || ordem == 0)
                    {
                        return ("", new { Erro = true, Mensagem = "Informar todos os parâmetros: ID,  DESCRICAO, ID_SITUACAO, ICONE e ORDEM" });
                    }

                    var dyParam = new OracleDynamicParameters();

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }

                    if (conn.State == ConnectionState.Open)
                    {
                        var query = "update grp_produtos set descricao = :descricao, id_situacao = :id_situacao, icone = :icone, " +
                                    "ordem = :ordem,  id_usuario_log = :id_usuario_login where id  = :id ";

                        dyParam.Add("id", id, OracleDbType.Int32, ParameterDirection.Input);
                        dyParam.Add("descricao", descricao, OracleDbType.NVarchar2, ParameterDirection.Input);
                        dyParam.Add("id_situacao", id_situacao, OracleDbType.Int32, ParameterDirection.Input);
                        dyParam.Add("icone", icone, OracleDbType.NVarchar2, ParameterDirection.Input);
                        dyParam.Add("ordem", ordem, OracleDbType.Int32, ParameterDirection.Input);
                        dyParam.Add("id_usuario_login", id_usuario_login, OracleDbType.Int32, ParameterDirection.Input);

                        var linhasafetadas = SqlMapper.Execute(conn, query, dyParam, null, null, CommandType.Text);

                        Utils.Atualizar_valor_config("ultima_atualizacao_produtos", DateTime.Now.ToString());

                        result = new { linhasafetadas };
                    }
                }
                catch (Exception ex)
                {
                    return ("", new { Erro = true, Mensagem = ex.Message });
                }

                return (result, new { Erro = false, Mensagem = "" });
            }
        }

        public object Consultar_ultima_atualizacao_produtos()
        {
            return (new { ultima_atualizacao_produtos = Utils.Consultar_valor_config("ultima_atualizacao_produtos")}, new { Erro = false, Mensagem = "" });
        }

    }
}
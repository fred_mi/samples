﻿namespace dw_webservice.Models
{
    public class NovoRegistro
    {
        public class Dados
        {
            public int Id { get; set; }
        }

        public class Retorno
        {
            public bool Erro { get; set; }
            public string Mensagem { get; set; }
        }

        public Dados Item1 { get; set; }
        public Retorno Item2 { get; set; }

        public NovoRegistro()
        {
            Item1 = new Dados();
            Item2 = new Retorno();
        }
    }
}

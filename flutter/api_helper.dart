import 'dart:async';
import 'dart:convert';
import 'package:samples/main.dart';
import 'package:samples/models/cidade_model.dart';
import 'package:samples/models/termo_servico_model.dart';
import 'package:http/http.dart' as http;
import 'package:samples/models/retorno_proc_model.dart';
import 'package:samples/models/usuario_model.dart';
import 'package:localstorage/localstorage.dart';

const URLDW = 'http://192.168.0.38:50001';
const TOKEN_ANONIMO = '44c2fc7b06e5b214c7e9febbf3c0a2487b83a8ed062c3a8df7f7617748a399db';
const TEMPO_VIDA_TOKEN = 25;
const ATIVO = "1";
const APLICATIVO = "A";
const SIM = "S";
const NAO = "N";

class ApiHelper {
  // Singleton
  static final ApiHelper _instance = ApiHelper.internal();
  LocalStorage storage = LocalStorage('');

  factory ApiHelper() => _instance;

  ApiHelper.internal();

  DateTime _ultima_chamada;

  Future<Usuario> loginUsuario(String email, String senha, String token_app) async {

    _ultima_chamada = DateTime.now();

    var _login = new Map();
    _login['email'] = email;
    _login['senha'] = senha;
    _login['token_app'] = token_app;
    _login['token'] = TOKEN_ANONIMO;

    final response =
        await http.post(URLDW + '/api/login_usuario', body: _login);

    if (response.statusCode == 200) {
      RetornoLogin _dados = RetornoLogin(json.decode(response.body));

      usuario.token = _dados.token;
      usuario.nome = _dados.nome;
      usuario.id = _dados.id;
      usuario.email = email;
      usuario.senha1 = senha;
      usuario.id = _dados.id;
      usuario.id_cidade_ap = _dados.id_cidade_ap;
      usuario.cidade = _dados.cidade;
      usuario.uf = _dados.uf;
      usuario.cpf = _dados.cpf;
      usuario.token_app = token_app;
      usuario.mensagem = _dados.mensagem;

      return usuario;
    } else {
      throw Exception('Erro crítico.');
    }
  }

  Future<RetornoInclusao> reinicarSenha(String email) async {

    _ultima_chamada = DateTime.now();

    var _login = new Map();
    _login['email'] = email;
    _login['token'] = TOKEN_ANONIMO;

    final response =
        await http.post(URLDW + '/api/reiniciar_senha', body: _login);

    if (response.statusCode == 200) {
      RetornoInclusao _dados = RetornoInclusao(json.decode(response.body));

      return _dados;
    } else {
      throw Exception('Erro crítico.');
    }
  }


  Future<List> consultarCidades({String uf = '', int id = 0, String nome = ''}) async {

    var _cidades = new Map();
    if (uf != '') {
      _cidades['uf'] = uf;
    }
    if (id != 0) {
      _cidades['id'] = id.toString();
    }
    if (nome != '') {
      _cidades['nome_exato'] = nome;
    }
    _cidades['token'] = TOKEN_ANONIMO;

    final response =
    await http.post(URLDW + '/api/consultar_cidade', body: _cidades);

    if (response.statusCode == 200) {
      RetornoCidade _dados = RetornoCidade(json.decode(response.body));
      if (_dados.erro) {
        throw Exception('Erro ao buscar os dados.');
      } else {
        return _dados.cidadeList;
      }
    } else {
      throw Exception('Erro crítico.');
    }
  }

  Future<RetornoInclusao> incluirUsuario(Usuario usuario) async {

    var _usuario = new Map();
    _usuario['email'] = usuario.email;
    _usuario['senha1'] = usuario.senha1;
    _usuario['senha2'] = usuario.senha2;
    _usuario['nome'] = usuario.nome;
    _usuario['cpf'] = usuario.cpf;
    _usuario['aplicativo'] = SIM;
    _usuario['retaguarda'] = NAO;
    _usuario['empresa'] = NAO;
    _usuario['id_situacao'] = ATIVO; //Ativo
    _usuario['id_cidade_ap'] = usuario.id_cidade_ap.toString();
    _usuario['token'] = TOKEN_ANONIMO;

    final response =
        await http.post(URLDW + '/api/incluir_usuario', body: _usuario);

    if (response.statusCode == 200) {
      RetornoInclusao _dados = RetornoInclusao(json.decode(response.body));

      return _dados;
    } else {
      throw Exception('Erro crítico.');
    }
  }

  Future<RetornoProcessamento> atualizarUsuario(Usuario usuario) async {

    await renovartoken();

    var _usuario = new Map();
    _usuario['id'] = usuario.id.toString();
    _usuario['email'] = usuario.email;
    _usuario['nome'] = usuario.nome;
    _usuario['cpf'] = usuario.cpf;
    _usuario['aplicativo'] = SIM;
    _usuario['retaguarda'] = NAO;
    _usuario['empresa'] = NAO;
    _usuario['id_situacao'] = ATIVO; //Ativo
    _usuario['id_cidade_ap'] = usuario.id_cidade_ap.toString();
    _usuario['token'] = usuario.token;

    final response =
    await http.post(URLDW + '/api/atualizar_usuario', body: _usuario);

    if (response.statusCode == 200) {
      RetornoProcessamento _dados = RetornoProcessamento(json.decode(response.body));

      return _dados;
    } else {
      throw Exception('Erro crítico.');
    }
  }

  Future<String> consultarTermoServico() async {

    var _termo = new Map();
    _termo['token'] = TOKEN_ANONIMO;

    final response =
        await http.post(URLDW + '/api/consultar_termo_servico', body: _termo);

    if (response.statusCode == 200) {
      RetornoTermoServico _dados =
          RetornoTermoServico(json.decode(response.body));
      if (_dados.erro) {
        throw Exception('Erro ao buscar os dados.');
      } else {
        return _dados.texto;
      }
    } else {
      throw Exception('Erro crítico.');
    }
  }

  Future<RetornoProcessamento> trocarSenha(String email, String senha_atual, String senha_nova, String senha_nova_conf) async {

    await renovartoken();

    var _trocar = new Map();
    _trocar['email'] = email;
    _trocar['senha_atual'] = senha_atual;
    _trocar['senha_nova'] = senha_nova;
    _trocar['senha_nova_conf'] = senha_nova_conf;
    _trocar['token'] = usuario.token;

    final response = await http.post(URLDW + '/api/trocar_senha',
        body: _trocar);

    if (response.statusCode == 200) {
      RetornoProcessamento _dados =
      RetornoProcessamento(json.decode(response.body));

      return _dados;
    } else {
      throw Exception('Erro crítico.');
    }
  }

  Future renovartoken() async {

    if  (usuario.token.isNotEmpty)
    {
      Duration difference = DateTime.now().difference(_ultima_chamada);
      if (difference.inMinutes >= TEMPO_VIDA_TOKEN)
      {
        await loginUsuario(usuario.email, usuario.senha1, usuario.token_app);
      }
    }
    _ultima_chamada = DateTime.now();
  }

}

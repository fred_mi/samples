import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class RetornoLogin
{
  int id = 0;
  String token = '';
  String nome  = '';
  int id_cidade_ap = 0;
  String cidade = '';
  String uf = '';
  String cpf = '';
  bool erro;
  String mensagem = '';

  RetornoLogin(Map dados)
  {
    erro = dados['item2']['erro'];

    if(!erro)
    {
      id = dados['item1']['id'];
      token = dados['item1']['token'];
      nome = dados['item1']['nome'];
      cpf = dados['item1']['cpf'];
      id_cidade_ap = dados['item1']['id_cidade_ap'];
      cidade = dados['item1']['cidade'];
      uf = dados['item1']['uf'];
    }

    mensagem = dados['item2']['mensagem'];
  }

}

class Usuario
{
  int id;
  String nome;
  String email;
  String senha1;
  String senha2;
  String cpf;
  String aplicativo;
  int id_cidade_ap;
  String cidade;
  String uf;
  String token;
  String token_app;
  String mensagem;

  void limpar()
  {
    final _storage = new FlutterSecureStorage();
    _storage.deleteAll();
    id = 0;
    nome = '';
    email = '';
    senha1 = '';
    senha2 = '';
    cpf = '';
    aplicativo = '';
    id_cidade_ap = 0;
    cidade = '';
    uf = '';
    token = '';
    mensagem = '';
  }
}